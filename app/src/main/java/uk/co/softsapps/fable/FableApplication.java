package uk.co.softsapps.fable;

import android.app.Application;

/**
 * Created by fernando on 01/08/2017.
 */

public class FableApplication extends Application {

    private static FableApplication sInstance;

    //TODO: Replace with userManager
    public String tempName;

    public static FableApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }
}
