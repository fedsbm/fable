package uk.co.softsapps.fable.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import uk.co.softsapps.fable.FableApplication;
import uk.co.softsapps.fable.R;
import uk.co.softsapps.fable.ui.credentials.CredentialsActivity;

/**
 * Created by fernando on 01/08/2017.
 */

public class MainMenuActivity extends AppCompatActivity {


    public static Intent getNewIntent(Context context) {
        Intent intent = new Intent(context, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button button = (Button) findViewById(R.id.button);
        TextView textView = (TextView) findViewById(R.id.text);
        textView.setText(FableApplication.getInstance().tempName);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(CredentialsActivity.getNewIntent(MainMenuActivity.this));
            }
        });
    }

}
